module.exports = {
    Uppy: require('uppy/lib/core'),
    Tus: require('uppy/lib/plugins/Tus'),
    Dashboard: require('uppy/lib/plugins/Dashboard')
}