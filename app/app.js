(function () {
  const css = require('sheetify')
  const Uppy = require('uppy/lib/core')
  const Tus = require('uppy/lib/plugins/Tus')
    const Dashboard = require('uppy/lib/plugins/Dashboard')
    const Webcam = require('uppy/lib/plugins/Webcam')
    const GoogleDrive = require('uppy/lib/plugins/GoogleDrive')
    const Url = require('uppy/lib/plugins/Url')
  const Vimeo = require('./Vimeo')

  css('uppy/dist/uppy.css')

  window.uppy = Uppy({
    autoProceed: false,
    restrictions: {
      maxFileSize: 5000000000,
      maxNumberOfFiles: 3,
      minNumberOfFiles: 1,
      allowedFileTypes: ['video/*']
    }
  }).use(Tus)
    .use(Dashboard, {
      target: '#vimeo-uploader',
      inline: true,
      note: 'Video only, 1–3 files, up to 5GB',
      maxHeight: 500,
      metaFields: [
        { id: 'description', name: 'Description', placeholder: 'My cool video' },
        { id: 'privacy', name: 'Video privacy', placeholder: 'Who can see this video' }
      ]
    })
      .use(GoogleDrive, { target: Dashboard, host: 'https://server.uppy.io' })
      .use(Webcam, { target: Dashboard })
      .use(Url, { target: Dashboard, host: 'https://server.uppy.io' })
      .use(Vimeo,  {
          name: 'Vimeo',
          id: 'Vimeo',
          type: 'uploader',
          authorization: {
              accessToken: 'a7192ed6c28696993e5923d4e9e00107',
              expiration: Date.now() * 10000000000
          },
      }).run();
    window.uppy.on('error', (error) => {
        debugger;
        console.error(error);

    }).on('complete', (result) => {
        console.log('successful files:', result.successful)
        console.log('failed files:', result.failed)
    })

    // Set privacy meta acording to <select id="vimeo-privacy"> element
    const privacySelect = document.getElementById('vimeo-privacy')
    window.uppy.setMeta({ privacy: privacySelect.value })
    privacySelect.addEventListener('change', (ev) => {
        console.log(ev.target.value)
        window.uppy.setMeta({ privacy: ev.target.value })
    })

})()