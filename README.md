# Vimeo Plugin for Uppy.js

Based on the solution found on this repository https://github.com/transloadit/uppy-vimeo-thing.git

This solution creates a plugin to be included on the uppy.js component, enabling the possibility to upload video to vimeo.
It was modified to receive the authorization data at the moment of creation on the constructor.
An example can be found on the folder example and below.

Preparing the env:
```
npm install
```

Execution: 
```
npm run dev
```

Build all bundles:
```
npm run build
```

Build uppy.js core bundle:
```
npm run build-uppy
```

Build vimeo plugin bundle:
```
npm run build-vimeo
```

The output files will be found on the folder dist[./dist]